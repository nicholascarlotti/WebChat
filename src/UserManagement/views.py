from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.db import IntegrityError
from django.contrib.auth import login, authenticate, logout
from .models import ChatUser
from django.contrib.auth.decorators import login_required
from RoomManagement.models import Room
import redis

cache = redis.Redis("localhost")


def user_registration(request):
    """
    TODO:
        Write registration template.
        Use https.
    """
    if request.method == "GET":
        # Return the registration form
        return render(request, "UserManagement/registration.html")
    else:
        data = request.POST
        username = data.get("username")
        email = data.get("email")
        password = data.get("password")
        try:
            print("Creating user {}".format(username))
            u = User.objects.create_user(
                username=username, email=email, password=password)
            cu = ChatUser(user=u, username=username)
            cu.save()
            # Add a default room
            default_room = Room.objects.get(room_name="WebChat1")
            cu.followed_rooms.add(default_room)
            return HttpResponse("OK")
        except IntegrityError:
            return HttpResponse(content="That username was already taken")


def user_login(request):
    if request.user.is_authenticated:
        return HttpResponse("You are already logged in")
    if request.method == "POST":
        data = request.POST
        user = authenticate(username=data.get("username"),
                            password=data.get("password"))
        if not user:
            # Send a login error response
            return HttpResponse(content="Wrong credentials")
        else:
            # Redirect to the chat picking page
            login(request, user)
            return redirect("/home/")
    else:
        # Serve an error page
        return render(request, "UserManagement/login.html")


@login_required(login_url="/login/")
def user_logout(request):
    if request.user.is_authenticated:
        chatUser = request.user.chatuser
        for followed_room in chatUser.followed_rooms.all():
            cache.srem(followed_room.room_name + "_users", chatUser.username)
        logout(request)
        return HttpResponse("You have been logged out.")
    else:
        return HttpResponse("You aren't logged in.")
