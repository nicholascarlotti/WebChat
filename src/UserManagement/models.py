from django.db import models
from django.contrib.auth.models import User
from RoomManagement.models import Room

# Create your models here.


class RoomLimitExceeded(Exception):
    pass


class ChatUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    followed_rooms = models.ManyToManyField(Room, blank=True)
    username = models.CharField(max_length=20)

    def __str__(self):
        return self.user.username

    def create_new_room(self, room_name, room_label=None):
        # A user can only own 3 rooms.
        created_rooms = self.room_set.all()
        if len(created_rooms) > 3:
            raise RoomLimitExceeded(
                "A simple user cannot own more than 3 rooms!")
        new_room = Room(
            room_name=room_name,
            room_label=room_label,
            room_creator=self
        )
        new_room.save()
        self.followed_rooms.add(new_room)
