from django.conf.urls import url
from .views import create_chat_room


urlpatterns = [
    url(r'^create_room/(?P<room_name>\w{1,20})/', create_chat_room),
]
