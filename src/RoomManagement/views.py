from django.shortcuts import render
from .models import Room
from django.http import HttpResponse
from django.db import IntegrityError


# Create your views here.


def create_chat_room(request, room_name):
    """
    TODO:
        Check for duplicates
    """
    room_label = request.POST.get("room_label")
    print("Received a request to add a new room")
    print("Room info:{}\n{}".format(room_name, room_label))
    new_room = Room(room_name=room_name, room_label=room_label)
    try:
        new_room.save()
    except IntegrityError:
        return HttpResponse(content="A room with that name already exists")
    print("Room added")
    return HttpResponse(content="OK")
