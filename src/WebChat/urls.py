"""WebChat URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from .views import user_follow_room, home_page, user_unfollow_room, online_users


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include("UserManagement.urls")),
    url(r'^rooms/', include("RoomManagement.urls")),
    url(r"^subtoroom/", user_follow_room),
    url(r"^home/", home_page),
    url(r"unsubtoroom/", user_unfollow_room),
    url(r"online_users/(?P<room_name>[\w\d]{1,36})/", online_users),

]
