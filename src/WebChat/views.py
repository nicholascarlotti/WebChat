from django.http import HttpResponse, Http404
from django.contrib.auth.models import User
from django.db import IntegrityError
from UserManagement.models import ChatUser
from RoomManagement.models import Room
from django.contrib.auth.decorators import login_required
from django.db.models import ObjectDoesNotExist
from django.shortcuts import render
import json
import redis


cache = redis.Redis("localhost")
# Initialize cache - should be externed in the future
# for room in Room.objects.all():
#     cache.sadd(room.room_name + "_users", None)


@login_required(login_url="/login/")
def user_follow_room(request):
    try:
        username = request.user.username
        user = ChatUser.objects.get(username=username)
        room_name = request.POST.get("room_name")
        print("Received a request to add {} to {}".format(username, room_name))
        room = Room.objects.get(room_name=room_name)
        user.followed_rooms.add(room)
    except ObjectDoesNotExist:
        return HttpResponse("Invalid username, room name or request type")
    return HttpResponse(content="OK")


@login_required(login_url="/login/")
def user_unfollow_room(request):
    try:
        user = request.user
        chat_user = ChatUser.objects.get(username=user.username)
        room_name = request.POST.get("room_name")
        room = Room.objects.get(room_name=room_name)
        chat_user.followed_rooms.remove(room)
    except ObjectDoesNotExist:
        return HttpResponse("Invalid user, room name or request type")
    return HttpResponse(
        "User {} removed from {}".format(user.username, room.room_name))


@login_required(login_url="/login/")
def home_page(request):
    # Return all the rooms the current
    # user is subscribed to and render the
    # chat dashboard page.
    try:
        chat_user = ChatUser.objects.get(username=request.user.username)
        followed_rooms = chat_user.followed_rooms
        room_names = []
        for room in followed_rooms.all():
            room_names.append(room.room_name)
        print("User {} is following:\n{}".format(chat_user.username,
                                                 room_names))
        context = {
            "followed_rooms": room_names,
        }

        return render(request, "WebChat/webchat-home.html", context=context)
    except ObjectDoesNotExist:
        return HttpResponse("Invalid user or room name.")


@login_required(login_url="/login/")
def online_users(request, room_name):
    if not Room.objects.all().filter(room_name=room_name).exists():
        raise Http404("The room you searched was not found")
    cache.sadd(room_name + "_users", request.user.username)
    online_users = cache.smembers("{}_users".format(room_name))
    online_users = list(map(lambda x: x.decode("utf-8"), online_users))
    response = {
        "online_users": online_users,
    }
    response = json.dumps(response)
    return HttpResponse(response)


@login_required(login_url="/login/")
def create_new_room(request):
    room_name = request.POST.get("room_name")
    room_label = request.POST.get("room_label")
    if  Room.objects.all().filter(room_name=room_name).exists():
        raise Http404("The room name you chose is not avilabile.")
    user = request.user
    chat_user = user.chatuser
    try:
        chat_user.create_new_room(room_name, room_label)
    except RoomLimitExceeded as ex:
        raise Http404(ex.message)
    return HttpResponse("Room created")