from .consumers import ws_message, ws_add, ws_disconnect
from channels.routing import route

channel_routing = [
    route("websocket.receive", ws_message),
    route("websocket.connect", ws_add),
    route("websocket.disconnect", ws_disconnect),

]
